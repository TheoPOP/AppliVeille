
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
  <meta charset="utf-8">
  <title>Accueil - Api Veille</title>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" href="css/style.css" media="screen" title="no title">
    </head>


<?php session_start(); // ici on continue la session
if ((!isset($_SESSION['email'])) || ($_SESSION['email'] == ''))
{
	// La variable $_SESSION['login'] n'existe pas, ou bien elle est vide
	// <=> la personne ne s'est PAS connectée
	echo '
  <div class=row>
  <div class=center-align>
  <a href="http://popveille.bross-victor.fr/index.html"><img src="template/img/stop.svg" alt="Forbidden !" class="center-align spin" style="height:30em"></a>
  <h2 class="center-align white-text">Hey petite fouine, tu dois te <a href="../index.php">connecter </a> pour accèder à l\'appli !</h2>.</p>
    </div>'."\n";
	exit();
} ?>


</html>
