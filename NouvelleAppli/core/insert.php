<?php
include ('template/head.php')
?>

<?php
//On verifie que le formulaire a ete envoye
if(isset($_POST['name'], $_POST['firstname'], $_POST['promo'], $_POST['email'], $_POST['pwd']) and $_POST['name']!='')
{
	//On enleve lechappement si get_magic_quotes_gpc est active
	if(get_magic_quotes_gpc())
	{
		$_POST['name'] = stripslashes($_POST['name']);
		$_POST['firstname'] = stripslashes($_POST['firstname']);
		$_POST['promo'] = stripslashes($_POST['promo']);
		$_POST['email'] = stripslashes($_POST['email']);
		$_POST['pwd'] = stripslashes($_POST['pwd']);
	}
	//On verifie si le mot de passe et celui de la verification sont identiques
	if($_POST['pwd']==$_POST['confirm'])
	{
		//On verifie si le mot de passe a 6 caracteres ou plus
		if(strlen($_POST['pwd'])>=6)
		{
			//On verifie si lemail est valide
			if(preg_match('#^(([a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+\.?)*[a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+)@(([a-z0-9-_]+\.?)*[a-z0-9-_]+)\.[a-z]{2,}$#i',$_POST['email']))
			{
				//On echape les variables pour pouvoir les mettre dans une requette SQL
				$name = mysql_real_escape_string($_POST['name']);
				$firstname = mysql_real_escape_string($_POST['firstname']);
				$promo = mysql_real_escape_string($_POST['promo']);
				$email = mysql_real_escape_string($_POST['email']);
				$pwd = mysql_real_escape_string($_POST['pwd']);

				//On verifie sil ny a pas deja un utilisateur inscrit avec le pseudo choisis
				$dn = mysql_num_rows(mysql_query('select id from users where email="'.$email.'"'));
				if($dn==0)
				{
					//On recupere le nombre dutilisateurs pour donner un identifiant a lutilisateur actuel
					$dn2 = mysql_num_rows(mysql_query('select id from users'));
					$id = $dn2+1;
					//On enregistre les informations dans la base de donnee
					if(mysql_query('insert into users(id, name, firstname, pwd, email, promo) values ('.$id.', "'.$name.'", "'.$firstname.'", "'.$pwd.'", "'.$email.'", "'.$promo.'")'))
					{
						//Si ca a fonctionne, on naffiche pas le formulaire
						$form = false;
            echo "<div class=valign-wrapper>
            <h3 class=center-align white-text>T'es bien inscrit, tu peux te <a href=template/connexion.php>connecter</a> et poster ta superbe veille !</h3></div>";
?>
<?php
					}
					else
					{
						//Sinon on dit quil y a eu une erreur
						$form = true;
						$message = 'Une erreur est survenue lors de l\'inscription.';
					}
				}
				else
				{
					//Sinon, on dit que le pseudo voulu est deja pris
					$form = true;
					$message = 'Un autre utilisateur utilise d&eacute;j&agrave; le nom d\'utilisateur que vous d&eacute;sirez utiliser.';
				}
			}
			else
			{
				//Sinon, on dit que lemail nest pas valide
				$form = true;
				$message = 'L\'email que vous avez entr&eacute; n\'est pas valide.';
			}
		}
		else
		{
			//Sinon, on dit que le mot de passe nest pas assez long
			$form = true;
			$message = 'Le mot de passe que vous avez entr&eacute; contien moins de 6 caract&egrave;res.';
		}
	}
	else
	{
		//Sinon, on dit que les mots de passes ne sont pas identiques
		$form = true;
		$message = 'Les mots de passe que vous avez entr&eacute; ne sont pas identiques.';
	}
}
else
{
	$form = true;
}
if($form)
{
	//On affiche un message sil y a lieu
	if(isset($message))
	{
		echo '<div class="message">'.$message.'</div>';
	}
	//On affiche le formulaire
	
	include ('template/signupform.php');
?>

<?php
}
?>
