<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Accueil - Api Veille</title>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" href="css/style.css" media="screen" title="no title">
</head>
<body>
  <div class="row">
    <div class="col m6 hide-on-small-only arrow transition">
      <a class="btn-floating btn-large waves-effect waves-light red lighten-2" href="index.html"><i class="material-icons">arrow_back</i></a>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col s12 m6 offset-m3">
        <div class="card card-others white z-depth-3">
          <div class="card-content black-text">
            <h4 class="center">Inscription</h4>
            <div class="row">
              <div class="col s12">
                <div class="row">
                  <div class="input-field col s6">
                    <form action="inscription.php" method="post">
                     <label for="name">Nom</label>
                    <input type="text" name="name" class="validate">
                  </div>
                  <div class="input-field col s6">
                     <label for="firstname">Prénom</label>
                     <input type="text" name="firstname" class="validate">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <label for="promo">Promotion</label>
                    <input type="text" name="promo" class="validate">
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                   <label for="email">Email</label>
                    <input type="email" name="email" class="validate">
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                   <label for="pwd">Mot de passe</label>
                     <input type="password" name="pwd" class="validate">
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                   <label for="confirm">Confirmez votre mot de passe</label>
                     <input type="password" name="confirm" class="validate">
                </div>
              </div>
              <div class="bouton center">
                <button class="btn waves-effect waves-light  cyan darken-1 " type="submit" name="action">Envoyer
                  <i class="material-icons right">send</i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
$handle = mysqli_connect('https://mysql11.lwspanel.com','bross740991','popveillephp','bross740991') or die(mysql_error());

//On verifie que le formulaire a ete envoye
if(isset($_POST['name'], $_POST['firstname'], $_POST['pwd'], $_POST['confirm'], $_POST['email'], $_POST['promo']) !='')
{
  //On enleve lechappement si get_magic_quotes_gpc est active
  if(get_magic_quotes_gpc())
  {
    $_POST['name'] = stripslashes($_POST['name']);
    $_POST['firstname'] = stripslashes($_POST['firstname']);
    $_POST['promo'] = stripslashes($_POST['promo']);
    $_POST['email'] = stripslashes($_POST['email']);
    $_POST['pwd'] = stripslashes($_POST['pwd']);
    $_POST['confirm'] = stripslashes($_POST['confirm']);
  }
  //On verifie si le mot de passe et celui de la verification sont identiques
  if($_POST['pwd']==$_POST['confirm'])
  {
    //On verifie si le mot de passe a 6 caracteres ou plus
    if(strlen($_POST['pwd'])>=6)
    {
      //On verifie si lemail est valide
      if(preg_match('#^(([a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+\.?)*[a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+)@(([a-z0-9-_]+\.?)*[a-z0-9-_]+)\.[a-z]{2,}$#i',$_POST['email']))
      {
        //On echape les variables pour pouvoir les mettre dans une requette SQL
        $password = mysql_real_escape_string($_POST['pwd']);
        $email = mysql_real_escape_string($_POST['email']);
        //On verifie sil ny a pas deja un utilisateur inscrit avec le pseudo choisis
        $dn = mysql_num_rows(mysql_query('select id from users where firstname="'.$firstname.'"'));
        if($dn==0)
        {
          //On recupere le nombre dutilisateurs pour donner un identifiant a lutilisateur actuel
          $dn2 = mysql_num_rows(mysql_query('select id from users'));
          $id = $dn2+1;
          //On enregistre les informations dans la base de donnee
          if(mysql_query('insert into users(id, name, firstname, promo, email, pwd) values ('.$id.', "'.$name.'", "'.$firstname.'", "'.$promo.'", "'.$email.'", "'.$pwd.'", "'.time().'")'))
          {
            //Si ca a fonctionne, on naffiche pas le formulaire
            $form = false;
?>
<div class="message">Vous avez bien été inscrit. Vous pouvez dor&eacute;navant vous connecter.<br />
<a href="connexion.php">Se connecter</a></div>
<?php
          }
          else
          {
            //Sinon on dit quil y a eu une erreur
            $form = true;
            $message = 'Une erreur est survenue lors de l\'inscription.';
          }
        }
        else
        {
          //Sinon, on dit que le pseudo voulu est deja pris
          $form = true;
          $message = 'Un autre utilisateur utilise d&eacute;j&agrave; le nom d\'utilisateur que vous d&eacute;sirez utiliser.';
        }
      }
      else
      {
        //Sinon, on dit que lemail nest pas valide
        $form = true;
        $message = 'L\'email que vous avez entr&eacute; n\'est pas valide.';
      }
    }
    else
    {
      //Sinon, on dit que le mot de passe nest pas assez long
      $form = true;
      $message = 'Le mot de passe que vous avez entr&eacute; contien moins de 6 caract&egrave;res.';
    }
  }
  else
  {
    //Sinon, on dit que les mots de passes ne sont pas identiques
    $form = true;
    $message = 'Les mots de passe que vous avez entr&eacute; ne sont pas identiques.';
  }
}
else
{
  $form = true;
}
if($form)
{
  //On affiche un message sil y a lieu
  if(isset($message))
  {
    echo '<div class="message">'.$message.'</div>';
  }
  //On affiche le formulaire
?>
</body>
</html>
