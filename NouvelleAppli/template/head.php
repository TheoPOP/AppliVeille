<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Accueil - Api Veille</title>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="../template/js/script.js"></script>
  <script type="text/javascript" src="../template/js/materialize.min.js"></script>
  <link type="text/css" rel="stylesheet" href="../template/css/materialize.min.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" href="../template/css/style.css" media="screen" title="no title">
</head>
