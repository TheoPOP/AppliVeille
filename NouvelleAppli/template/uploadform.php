<div class="row">
          <div class="card-panel white">
            <h3 class="center titre">Poste ta veille ici </h3>
            <form class="center" action="../core/upload.php" method="post" enctype="multipart/form-data">


              <div class="row">

                  <div class="row">
                    <div class="input-field col s12 offset-md3">
                      <input id="input_text" type="text" name="titre" length="65">
                      <label for="input_text">Titre de ta veille</label>
                    </div>
                    <div class="row">
                      <div class="input-field col s12 offset-md3">
                        <input type="text" name="date" class="datepicker">
                        <label for="date">Date de ta veille</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12 offset-md3">
                        <select multiple name="type">
                          <option value="general" data-icon="template/img/earth.svg">Général</option>
                          <option value="hardware" data-icon="template/img/cpu-64.svg">Hardware</option>
                          <option value="software" data-icon="template/img/browser-apps.svg">Software</option>
                          <option value="objet connecte" data-icon="template/img/ram-card.svg">Objet connecté</option>
                          <option value="juridique"  data-icon="template/img/scale.svg">Juridique</option>
                          <option value="securite"  data-icon="template/img/network-firewall off.svg">Sécurité</option>
                          <option value="hacking"  data-icon="template/img/unlocked.svg">Hacking</option>
                          <option value="dev" data-icon="template/img/browser-code.svg">Dev.</option>
                          <option value="vie privee" data-icon="template/img/cookie.svg">Vie privée</option>
                          <option value="kickstarter" data-icon="template/img/kickstarter.svg">Kickstarter</option>
                        </select>
                        <label>Le / les type(s) de ta veille</label>
                      </div>
                    </div>
                    <div class="row">

                      <div class="file-field input-field col s10 offset-md3">
                        <div class="btn">
                          <span>Fichiers</span>
                          <i class="material-icons right">publish</i>
                          <input type="hidden" name="MAX_FILE_SIZE" value="1000000000">
                          <input type="file" name="file">
                        </div>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text" placeholder="Upload ton fichier de veille (pdf, odt, doc ...)">
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                <div class="row center-align">

                  <button class="btn waves-effect waves-light" type="submit" name="action">Publier
                  </button>
                </div>

              </form>

            </div>

          </div>
