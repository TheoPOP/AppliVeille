  <header>
    <nav class="top-nav white">
      <div class="poprandom">
        <a class="waves-effect waves-light btn modal-trigger right" href="#modal1"><span class="fa fa-refresh fa-spin"></span><span class="hide-on-small-only">À qui ?</span></a>

      </div>
      <a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only ">
        <i class="material-icons burger cyan darken-3">menu</i>
      </a>


      <div class="nav-wrapper container center">

        <h5 class="titre">Accueil</h5>

      </div>

    </nav>


    <ul id="nav-mobile" class="side-nav fixed blue-grey darken-3">
      <li class="logo">
      <object class="pop-logo center"data="../popschool.svg" type="image/svg+xml"></object>
      </li>
      <li class="center welcome white-text">

        Bienvenue <?php if(isset($_SESSION['firstname'])){echo ' '.htmlentities($_SESSION['firstname'], ENT_QUOTES, 'UTF-8');} ?>
        <br> <form class="" action="../core/deco.php" method="post">

        <button type="submit" name="button" name="déco" class="waves-effect waves-light red lighten-2 btn">Se déconnecter</button></form>
      </li>
      <li class="bold"> <a href="../indexappli.php" class="waves-effect waves-cyan">Accueil</a></li>
      <li class="bold"> <a href="../template/addveilles.php" class="waves-effect waves-cyan ">Poster une veille</a></li>
      <li class="bold"> <a href="../template/mesveilles.php" class="waves-effect waves-cyan ">Mes veilles</a></li>
    </ul>
  </header>
