<div class="row">
    <div class="col m6 hide-on-small-only arrow transition">
         <a class="btn-floating btn-large waves-effect waves-light red lighten-2" href="../index.php"><i class="material-icons">arrow_back</i></a>
    </div>
    <div class="container">
    <div class="row">
      <div class="col s12 m6 offset-m3">
        <div class="card card-others white z-depth-3">
          <div class="card-content black-text">
            <h4 class="center">Connexion</h4>
            <div class="row">
              <div class="col s12">
                <form action="connexion.php" method="post">
                <div class="row">
                  <div class="input-field col s12">
                    <input id="email" type="text" name="email" class="validate">
                    <label for="email">E-mail</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <input id="pwd" type="password" name="pwd" class="validate">
                    <label for="pwd">Mot de passe</label>
                  </div>
                </div>
                <div class="bouton center">
                  <button class="btn waves-effect waves-light  cyan darken-1 " type="submit" name="action">Envoyer
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
