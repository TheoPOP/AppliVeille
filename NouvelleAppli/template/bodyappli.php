 <main>
    <div class="row">
      <div class="col s12 m8">
        <div class="card-panel white">
          <span class="contenu">
            <h3 class="center titre">Les dernières veilles</h3>
            <table class=" centered">
              <thead>
                <tr>
                  <th data-field="name">Auteur</th>
                  <th data-field="id">Titre</th>
                  <th data-field="name">Type</th>
                  <th data-field="name">Date de publication</th>

                </tr>
              </thead>


              <tbody>
               <?php


                 // lancement de la requete
	              // SQL query
	              $strSQL = 'SELECT * FROM veille ORDER BY date DESC';

	              // Execute la requete ($rs contiens le resultat)
	            $rs = mysql_query($strSQL);

	            //chaque colonne sera mis dans un tableau $rs
	            while($row = mysql_fetch_array($rs)) {

              ?>
                <tr>
                  <td><?php echo $row['firstname']; ?></td>
                  <td><?php echo $row['titre']; ?></td>
                  <td><?php echo $row['type']; ?></td>
                  <td><?php echo $row['date']; ?></td>

                </tr>
                <?php
                      }
                ?>
              </tbody>

            </table>
          </span>
        </div>

        <?php
          include ('template/uploadform.php');
        ?>
        </div>


        <?php
          include ('template/actu.php');
        ?>
        </div>
        <div id="modal1" class="modal bottom-sheet">

          <div class="modal-content">
            <h4>Randomizator - A qui le tour ?</h4>
            <p>Cliquez sur le logo à droite pour tirer au sort. Qui passera en veille ? Qui passera le balais ? Ca se joue ici ! </p>

            <ul class="collection">
              <li class="collection-item avatar">
                <img src="template/img/projector.svg" alt="" class="circle">
                <span class="title">Veille</span>

                <p id="text"></p>
                <a id="button"  class="secondary-content"><i class="material-icons spin">loop</i></a>
                <script>
      $(function() {
        $('#button').click(function() {
          $.ajax({
            type: 'GET',
            url: 'core/random.php',
            timeout: 3000,
            success: function(data) {
              $('#text').text(data); },
            error: function() {
              alert('La requête n\'a pas abouti'); }
          });
        });
      });
      </script>
              </li>
              <li class="collection-item avatar">
                <img src="template/img/broom.svg" alt="" class="circle">
                <span class="title">Balais</span>

                <p id="text1"></p>
                <a id="button1"  class="secondary-content"><i class="material-icons spin">loop</i></a>
                <script>
      $(function() {
        $('#button1').click(function() {
          $.ajax({
            type: 'GET',
            url: 'core/random.php',
            timeout: 3000,
            success: function(data) {
              $('#text1').text(data); },
            error: function() {
              alert('La requête n\'a pas abouti'); }
          });
        });
      });
      </script>
              </li>
              <li class="collection-item avatar">
                <img src="template/img/faucet-2.svg" alt="" class="circle">
                <span class="title">Vaisselle</span>
                <p id="text2"></p>
                <a id="button2"  class="secondary-content"><i class="material-icons spin">loop</i></a>
                <script>
      $(function() {
        $('#button2').click(function() {
          $.ajax({
            type: 'GET',
            url: 'core/random.php',
            timeout: 3000,
            success: function(data) {
              $('#text2').text(data); },
            error: function() {
              alert('La requête n\'a pas abouti'); }
          });
        });
      });
      </script>
              </li>
              <li class="collection-item avatar">
                <img src="template/img/delete.svg" alt="" class="circle">
                <span class="title">Poubelle</span>
                <p id="text3"></p>
                <a id="button3"  class="secondary-content"><i class="material-icons spin">loop</i></a>
                <script>
      $(function() {
        $('#button3').click(function() {
          $.ajax({
            type: 'GET',
            url: 'core/random.php',
            timeout: 3000,
            success: function(data) {
              $('#text3').text(data); },
            error: function() {
              alert('La requête n\'a pas abouti'); }
          });
        });
      });
      </script>
              </li>
              <li class="collection-item avatar">
                <img src="template/img/piggy-bank.svg" alt="" class="circle">
                <span class="title">Popotier</span>
                <p id="text4"></p>
                <a id="button4"  class="secondary-content"><i class="material-icons spin">loop</i></a>
                <script>
      $(function() {
        $('#button4').click(function() {
          $.ajax({
            type: 'GET',
            url: 'core/random.php',
            timeout: 3000,
            success: function(data) {
              $('#text4').text(data); },
            error: function() {
              alert('La requête n\'a pas abouti'); }
          });
        });
      });
      </script>
              </li>
            </ul>
          </div>
        </div>

      </main>
