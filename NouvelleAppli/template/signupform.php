<div class="row">
   <div class="col m6 hide-on-small-only arrow transition">
     <a class="btn-floating btn-large waves-effect waves-light red lighten-2" href="index.php"><i class="material-icons">arrow_back</i></a>
   </div>
 </div>
 <div class="container">
   <div class="row">
     <div class="col s12 m6 offset-m3">
       <div class="card card-others white z-depth-3">
         <div class="card-content black-text">

           <h4 class="center">Inscription</h4>

           <div class="row">
             <div class="col s12">
               <div class="row">
                 <div class="input-field col s6">
                   <form action="sign_up.php" method="post">
                    <label for="name">Nom</label>
                   <input type="text" name="name" class="validate">

                 </div>
                 <div class="input-field col s6">
                    <label for="firstname">Prénom</label>
                    <input type="text" name="firstname" class="validate">
                 </div>
               </div>
             </div>
             <div class="row">
               <div class="input-field col s12">
                 <label for="promo">Promotion</label>
                   <input type="text" name="promo" class="validate">
               </div>
             </div>
             <div class="row">
               <div class="input-field col s12">
                  <label for="email" data-error="wrong" data-success="right">Email</label>
                   <input type="email" name="email" class="validate">
               </div>
             </div>
             <div class="row">
               <div class="input-field col s12">
                  <label for="pwd" data-error="wrong" data-success="right">Mot de passe</label>
                    <input type="password" name="pwd" class="validate">
               </div>
             </div>
             <div class="row">
               <div class="input-field col s12">
                  <label for="confirm" data-error="wrong" data-success="right">Confirmez votre mot de passe</label>
                    <input type="password" name="confirm" class="validate">
               </div>
             </div>
             <div class="bouton center">
               <button class="btn waves-effect waves-light  cyan darken-1 " type="submit" name="action">Envoyer
                 <i class="material-icons right">send</i>
               </button>
             </div>
           </div>
         </form>
       </div>
     </div>
   </div>
 </div>
</div>

