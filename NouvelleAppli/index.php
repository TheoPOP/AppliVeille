
<?php 
include('template/head.php')

?>

<body>

  <div class="container">

    <div class="row">
      <div class="valign-wrapper">
      <div class="col s12 m6 offset-m3 ">
        <div class="card white z-depth-3 ">
          <div class="card-content black-text">
            <div class="center">
              <img src="popschool.svg" alt="Pop" class="pop-logo" />
            </div>
            <h4 class="center hello">Salut veilleur !</h4>

            <p class="center">Bienvenue sur la page d'accueil de l'api pour poster ta veille chaque jour.<br> <span class="text">Tu es déjà inscrit ? <a href="template/connexion.php">Connecte toi !</a></span>
              <br>  Sinon inscris toi :)

            </p>
          </div>
          <div class="center">
            <div class="bouton">
              <a class="waves-effect waves-light btn teal accent-4 transition " href="sign_up.php"><i class="material-icons left">mode_edit</i>S'incrire</a>
            </div>
            <div class="bouton">
              <a class="waves-effect waves-light btn cyan transition" href="connexion.php"><i class="material-icons left">perm_identity</i>Se connecter</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
      </div>






<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>
