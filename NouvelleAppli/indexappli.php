<?php
 include('config/config.php');
 include('template/head.php');
 require('config/control-session.php');
?>

<body>
<?php
  include ('template/header.php');
  include ('template/bodyappli.php');
?>

  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="template/js/script.js"></script>
  <script type="text/javascript" src="template/js/materialize.js"></script>
  
</body>
</html>
